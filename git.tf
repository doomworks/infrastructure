locals {
  web_port = var.environment == "production" ? 443 : 80
  proto    = var.environment == "production" ? "https" : "http"
}

resource "docker_volume" "gitlab_data" {
  name = "${var.short_label}-gitlab-data"
}

resource "docker_volume" "gitlab_logs" {
  name = "${var.short_label}-gitlab-logs"
}

resource "docker_volume" "gitlab_config" {
  name = "${var.short_label}-gitlab-config"
}

resource "docker_secret" "gitlab_root_password" {
  name = "${var.short_label}-gitlab-root-password-${replace(timestamp(), ":", ".")}"
  data = base64encode(var.gitlab_root_password)
  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
}

resource "docker_config" "gitlab_config" {
  name = "${var.short_label}-gitlab-config-${replace(timestamp(), ":", ".")}"
  data = base64encode(join("\n", [
    "external_url '${local.proto}://git.${var.domain}'",
    "gitlab_rails['initial_root_password'] = File.read('/run/secrets/gitlab_root_password').gsub(\"\\n\", \"\")"
  ]))
  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
}

resource "docker_image" "gitlab_core" {
  name = "${var.label}/gitlab"
  build {
    path = "docker/gitlab/base/"
    tag  = ["${var.label}/gitlab:1.0.0"]
    label = {
      "author" = var.maintainer
    }
  }
}

resource "docker_image" "gitlab_runner" {
  name = "${var.label}/gitlab_runner"
  build {
    path = "docker/gitlab/runner/"
    tag  = ["${var.label}/gitlab_runner:1.0.0"]
    label = {
      "author" = var.maintainer
    }
  }
}

resource "docker_service" "gitlab" {
  name = "${var.short_label}-gitlab-service"
  task_spec {
    container_spec {
      image    = docker_image.gitlab_core.latest
      hostname = "${var.short_label}-gitlab"
      mounts {
        target = "/etc/letsencrypt/"
        source = docker_volume.certificates.name
        type   = "volume"
      }
      mounts {
        target = "/var/opt/gitlab"
        source = docker_volume.gitlab_data.name
        type   = "volume"
      }
      mounts {
        target = "/var/log/gitlab"
        source = docker_volume.gitlab_logs.name
        type   = "volume"
      }
      mounts {
        target = "/etc/gitlab"
        source = docker_volume.gitlab_config.name
        type   = "volume"
      }
      configs {
        config_id = docker_config.gitlab_config.id
        config_name = docker_config.gitlab_config.name
        file_name = "/omnibus_config.rb"
      }
      secrets {
        secret_id   = docker_secret.gitlab_root_password.id
        secret_name = docker_secret.gitlab_root_password.name
        file_name   = "/run/secrets/gitlab_root_password"
        file_uid    = "0"
        file_gid    = "0"
        file_mode   = 0777
      }
      env = {
        GITLAB_OMNIBUS_CONFIG = "from_file('/omnibus_config.rb')"
      }
    }
    networks = [docker_network.internal.id]
    restart_policy {
      condition    = "on-failure"
      delay        = "3s"
      max_attempts = 5
      window       = "10s"
    }
    runtime = "container"
  }
}

resource "docker_service" "gitlab_runner" {
  name = "${var.short_label}-gitlab-runner-service"
  task_spec {
    container_spec {
      image = docker_image.gitlab_runner.latest

      env = {
        "GITLAB_URL" = "${var.short_label}-gitlab"
      }
    }
    networks = [docker_network.internal.id]
    restart_policy {
      condition    = "on-failure"
      delay        = "3s"
      max_attempts = 5
      window       = "10s"
    }
  }
  mode {
    replicated {
      replicas = 4
    }
  }
}