locals {
  ldap_port    = var.environment == "production" ? 636 : 389
  ldap_base_dn = (var.domain == "localhost") ? "dc=ldap,dc=local" : "dc=${var.label},dc=${var.tld}"
  env = yamlencode({
    LDAP_ORGANISATION   = var.ldap_org,
    LDAP_DOMAIN         = (var.domain == "localhost") ? "ldap.local" : var.domain,
    LDAP_BASE_DN        = (var.domain == "localhost") ? "dc=ldap,dc=local" : "dc=${var.label},dc=${var.tld}",
    LDAP_ADMIN_PASSWORD = var.ldap_admin_password
  })
  env_prod = yamlencode({
    LDAP_TLS_CRT_FILENAME    = "/etc/letsencrypt/live/${var.domain}/cert.pem",
    LDAP_TLS_KEY_FILENAME    = "/etc/letsencrypt/live/${var.domain}/privkey.pem",
    LDAP_TLS_CA_CRT_FILENAME = "/etc/letsencrypt/live/${var.domain}/fullchain.pem"
  })
  env_dev     = yamlencode({ LDAP_TLS = false })
  env_ldap    = var.environment == "production" ? local.env_prod : local.env_dev
  env_ldap_ui = var.environment == "production" ? {} : { PHPLDAPADMIN_HTTPS = false }

  base_ldif = join("\n", [
    #"dn: ${local.ldap_base_dn}\ndc: ${var.domain == "localhost" ? "local" : var.label}\no: ${var.ldap_org}\nobjectClass: dcObject\nobjectClass: organization\n",
    "dn: cn=Manager,${local.ldap_base_dn}\ncn: Manager\ndescription: LDAP Administrator\nobjectClass: top\nobjectClass: organizationalRole\nroleOccupant: ${local.ldap_base_dn}\n",
    "dn: ou=People,${local.ldap_base_dn}\nou: People\nobjectClass: top\nobjectClass: organizationalUnit\n",
    "dn: ou=Group,${local.ldap_base_dn}\nou: Group\nobjectClass: top\nobjectClass: organizationalUnit\n"
  ])
  users_ldif = [for idx, user in var.ldap_user : join("\n", [
    "dn: uid=${user.uid},ou=People,${local.ldap_base_dn}",
    "uid: ${user.uid}",
    "cn: ${user.first_name} ${user.last_name}",
    "sn: ${user.last_name}",
    "o: ${var.ldap_org}",
    "ou: People",
    "givenName: ${user.first_name}",
    "userPassword: ${user.password}",
    "uidNumber: ${8001 + idx}",
    "gidNumber: ${8001 + idx}",
    "loginShell: /bin/bash",
    "homeDirectory: /home/${user.uid}",
    "mail: ${user.email}",
    "description: User Account for ${user.first_name} ${user.last_name}",
    "st: ${user.state}",
    "l: ${user.city}, ${user.country}",
    "objectClass: top",
    "objectClass: person",
    "objectClass: organizationalPerson",
    "objectClass: inetOrgPerson",
    "objectClass: posixAccount",
    "objectClass: shadowAccount"
  ])]
}

resource "docker_volume" "ldap_db" {
  name = "${var.short_label}-ldap-db"
}

resource "docker_volume" "ldap_config" {
  name = "${var.short_label}-ldap-config"
}

resource "docker_secret" "ldap_base_config" {
  name = "${var.short_label}-ldap-base-config-${replace(timestamp(), ":", ".")}"
  data = base64encode(local.env)
  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
}

resource "docker_secret" "ldap_tls_config" {
  name = "${var.short_label}-ldap-tls-config-${replace(timestamp(), ":", ".")}"
  data = base64encode(local.env_ldap)
  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
}

resource "docker_secret" "ldap_base_seed" {
  name = "${var.short_label}-ldap-base-seed-${replace(timestamp(), ":", ".")}"
  data = base64encode(local.env_ldap)
  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
}

resource "docker_secret" "ldap_user" {
  count = length(local.users_ldif)
  name  = "${var.short_label}-ldap-user-${count.index}-${replace(timestamp(), ":", ".")}"
  data  = base64encode(local.users_ldif[count.index])
  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
}

resource "docker_image" "ldap" {
  name = "${var.label}/ldap"
  build {
    path = "docker/ldap/"
    tag  = ["${var.label}/ldap:1.0.0"]
    build_arg = merge({
      "SEED"  = local.base_ldif
      "USERS" = join("\n", local.users_ldif)
    })
    label = {
      "author" = var.maintainer
    }
  }
}

resource "docker_image" "ldap_ui" {
  name = "${var.label}/ldap-ui"
  build {
    path = "docker/ldap-ui/"
    tag  = ["${var.label}/ldap-ui:1.0.0"]
    label = {
      "author" = var.maintainer
    }
  }
}

resource "docker_service" "ldap" {
  name = "${var.short_label}-ldap-service"
  task_spec {
    container_spec {
      image    = docker_image.ldap.latest
      hostname = "${var.short_label}-ldap"
      mounts {
        target = "/etc/letsencrypt/"
        source = docker_volume.certificates.name
        type   = "volume"
      }
      mounts {
        target = "/var/lib/ldap"
        source = docker_volume.ldap_db.name
        type   = "volume"
      }
      mounts {
        target = "/etc/ldap/slapd.d"
        source = docker_volume.ldap_config.name
        type   = "volume"
      }
      secrets {
        secret_id   = docker_secret.ldap_base_config.id
        secret_name = docker_secret.ldap_base_config.name
        file_name   = "/container/environment/01-custom/base.startup.yaml"
        file_uid    = "911"
        file_gid    = "911"
        file_mode   = 0777
      }
      secrets {
        secret_id   = docker_secret.ldap_tls_config.id
        secret_name = docker_secret.ldap_tls_config.name
        file_name   = "/container/environment/01-custom/tls.startup.yaml"
        file_uid    = "911"
        file_gid    = "911"
        file_mode   = 0777
      }
    }
    networks = [docker_network.internal.id]
    restart_policy {
      condition    = "on-failure"
      delay        = "3s"
      max_attempts = 5
      window       = "10s"
    }
    runtime = "container"
  }
  endpoint_spec {
    ports {
      target_port    = local.ldap_port
      publish_mode   = "host"
      published_port = local.ldap_port
    }
  }
}

resource "docker_service" "ldap_ui" {
  name = "${var.short_label}-ldap-ui-service"
  task_spec {
    container_spec {
      image    = docker_image.ldap_ui.latest
      hostname = "${var.short_label}-ldap-ui"
      env = merge({
        PHPLDAPADMIN_LDAP_HOSTS = "${var.short_label}-ldap"
      }, local.env_ldap_ui)
    }
    restart_policy {
      condition    = "on-failure"
      delay        = "3s"
      max_attempts = 5
      window       = "10s"
    }
    networks = [docker_network.internal.id]
    runtime  = "container"
  }
  depends_on = [
    docker_service.ldap
  ]
}