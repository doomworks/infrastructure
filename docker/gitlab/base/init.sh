#!/bin/bash

# Run Main Application
/assets/wrapper &

# Generate Token if it doesn't exit

if [ ! -f /runner-token ]; then
    # Wait 1m for the Runtime Configuration to be generated
    echo "[CUSTOM] Waiting 10m until generating Token."
    sleep 600s

    echo "[CUSTOM] Job List:"
    jobs -l

    # Retrieve Token
    echo "[CUSTOM] Generating Token!"
    gitlab-rails runner -e production \
        "puts Gitlab::CurrentSettings.current_application_settings.runners_registration_token" \
        > /runner-token
    echo "[CUSTOM] Token Generated!"
fi

fg %/assets/wrapper