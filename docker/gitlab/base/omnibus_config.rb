external_url '${proto}://git.${domain}'

gitlab_rails['initial_root_password'] = File.read('/run/secrets/gitlab_root_password').gsub("\n", "")
gitlab_rails['db_host'] = '127.0.0.1'