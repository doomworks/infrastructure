#!/bin/ash

URL="$([ "$ENVIRONMENT" == "prod" ] && echo "https" || echo "http")://${GITLAB_URL}"

while [ 1 ]; do
    wget \
        --spider \
        --retry-connrefused \
        --waitretry=1 \
        --read-timeout=20 \
        --timeout=15 -t 0 \
        --no-check-certificate \
        --continue \
        "$URL" && break || true;
    sleep 1s
done;

head -n-1 /entrypoint > /prepare.sh
chmod +x /prepare.sh
/prepare.sh

while [ 1 ]; do
    if [ -f /runner-token ]; then
        break;
    fi
    echo "Waiting for Runner Token!";
    sleep 5s;
done;

if [ ! -f /etc/gitlab-runner/config.toml ]; then
    gitlab-runner register \
        --non-interactive \
        --url "$URL" \
        --registration-token "$(cat /runner-token)" \
        --executor "docker" \
        --docker-image alpine:3 \
        --description "docker-runner" \
        --tag-list "docker" \
        --run-untagged \
        --locked="false" \
        --docker-privileged
fi

exec gitlab-runner run --user=gitlab-runner --working-directory=/home/gitlab-runner