#!/bin/bash

# Env. Substitution
export DOLLAR='$'
find /tmp/nginx -type f -print | sed 's/\/tmp\/nginx\/' | xargs -I '{}' envsubst < /tmp/nginx/'{}' > /etc/nginx/'{}'

# Docker Entrypoint
echo "Starting nginx..."
/docker-entrypoint.sh nginx -g 'daemon off;'
