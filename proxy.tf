locals {
  proxy_port = var.environment == "production" ? 443 : 80
}

resource "docker_volume" "nginx_conf" {
  name = "${var.short_label}-nginx-conf"
}

resource "docker_volume" "nginx_logs" {
  name = "${var.short_label}-nginx-logs"
}

resource "docker_network" "internal" {
  name   = "${var.label}-network"
  driver = "overlay"
}

resource "docker_image" "http_proxy" {
  name = "${var.label}/httpd"
  build {
    path = "docker/apache-proxy/"
    tag  = ["${var.label}/httpd:1.0.0"]
    build_arg = {
      PROXY_PORT : local.proxy_port
      DOMAIN : var.domain
    }
    label = {
      "author" = var.maintainer
    }
  }
}

resource "docker_image" "tcp_proxy" {
  name = "${var.label}/nginx"
  build {
    path = "docker/nginx/"
    tag  = ["${var.label}/nginx:1.0.0"]
    build_arg = {
      DOMAIN : var.domain
    }
    label = {
      "author" = var.maintainer
    }
  }
}

resource "docker_service" "http_proxy" {
  name = "${var.short_label}-http-proxy-service"
  task_spec {
    container_spec {
      image = docker_image.http_proxy.latest
      mounts {
        target = "/etc/letsencrypt/"
        source = docker_volume.certificates.name
        type   = "volume"
      }
      mounts {
        target = "/usr/local/apache2/conf/sites"
        source = "${abspath(path.root)}/docker/apache-proxy/site"
        type   = "bind"
      }
      mounts {
        target = "/usr/local/apache2/htdocs"
        source = "${abspath(path.root)}/docker/apache-proxy/pages"
        type   = "bind"
      }
      env = {
        DOMAIN       = var.domain,
        ENVIRONMENT  = var.environment,
        PROXY_PORT   = local.proxy_port,
        LDAP_HOST    = "${var.short_label}-ldap",
        LDAP_UI_HOST = "${var.short_label}-ldap-ui",
        GITLAB_HOST  = "${var.short_label}-gitlab"
      }
    }
    networks = [docker_network.internal.id]
    restart_policy {
      condition    = "on-failure"
      delay        = "3s"
      max_attempts = 5
      window       = "10s"
    }

    runtime = "container"
  }
  endpoint_spec {
    ports {
      target_port    = local.proxy_port
      publish_mode   = "host"
      published_port = local.proxy_port
    }
  }
  depends_on = [
    docker_service.certbot,
    docker_service.ldap
  ]
}

resource "docker_service" "tcp_proxy" {
  name = "${var.short_label}-tcp-proxy-service"
  task_spec {
    container_spec {
      image = docker_image.tcp_proxy.latest
      mounts {
        target = "/etc/nginx"
        source = docker_volume.nginx_conf.name
        type   = "volume"
      }
      mounts {
        target = "/var/log/nginx"
        source = docker_volume.nginx_logs.name
        type   = "volume"
      }
      env = {
        DOMAIN      = var.domain,
        GITLAB_HOST = "${var.short_label}-gitlab"
      }
    }
    networks = [docker_network.internal.id]
    restart_policy {
      condition    = "on-failure"
      delay        = "3s"
      max_attempts = 5
      window       = "10s"
    }

    runtime = "container"
  }
  endpoint_spec {
    # GitLab SSH
    ports {
      target_port    = 8022
      publish_mode   = "host"
      published_port = 8022
    }
  }
  depends_on = [
    docker_service.gitlab
  ]
}