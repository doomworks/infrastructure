resource "docker_image" "certbot" {
  name = "${var.label}/certbot"
  build {
    path = "docker/certbot/"
    tag  = ["${var.label}/certbot:1.0.0"]
    label = {
      "author" = var.maintainer
    }
  }
}

resource "docker_volume" "certificates" {
  name = "${var.short_label}-certificates"
}

resource "docker_secret" "cloudflare_token" {
  name = "${var.short_label}-cloudflare-token-${replace(timestamp(), ":", ".")}"
  data = base64encode("dns_cloudflare_api_token = ${var.cloudflare_token}")
  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
}

resource "docker_service" "certbot" {
  name = "${var.short_label}-certbot-service"
  task_spec {
    container_spec {
      image = docker_image.certbot.latest
      mounts {
        target = "/etc/letsencrypt/"
        source = docker_volume.certificates.name
        type   = "volume"
      }
      secrets {
        secret_id   = docker_secret.cloudflare_token.id
        secret_name = docker_secret.cloudflare_token.name
        file_name   = "/cloudflare.ini"
        file_uid    = "0"
        file_gid    = "0"
        file_mode   = 0777
      }
      env = {
        DOMAIN      = var.domain,
        ENVIRONMENT = var.environment
      }
    }
    restart_policy {
      condition    = "on-failure"
      delay        = "3s"
      max_attempts = 5
      window       = "10s"
    }
    runtime = "container"
  }
}
