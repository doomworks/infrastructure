##########################
# Deployment Environment #
##########################
variable "os" {
  type        = string
  description = "In case of Windows, please specify 'win', otherwise 'unix'."
  default     = "unix"
  validation {
    condition     = contains(["win", "unix"], var.os)
    error_message = "The operating system must be either 'win' or 'unix'"
  }
}

variable "environment" {
  type        = string
  description = "The deployment environment. One of: production, staging, develop"
  default     = "develop"
  validation {
    condition     = contains(["production", "staging", "develop"], var.environment)
    error_message = "The environment must be either 'production', 'staging' or 'develop'"
  }
}

variable "domain" {
  type        = string
  description = "The Host Domain"
  default     = "localhost"
}

variable "tld" {
  type        = string
  description = "The TLD of the Domain"
  default     = "localhost"
}

########################
# System Configuration #
########################
variable "ldap_org" {
  type        = string
  description = "LDAP Organization"
}

variable "ldap_user" {
  type = list(object({
    uid        = string
    first_name = string
    last_name  = string
    password   = string
    email      = string
    state      = string
    city       = string
    country    = string
  }))
}

##########
# Labels #
##########
variable "label" {
  type        = string
  description = "Your company's name. Snake-case is required."
  validation {
    condition     = can(regex("[_A-Za-z0-9]+", var.label))
    error_message = "The label has to be a snake case word."
  }
}

variable "short_label" {
  type        = string
  description = "The short version of your company's name. One word, lowercase abbreviation is required."
  validation {
    condition     = can(regex("[a-z0-9]+", var.short_label))
    error_message = "The short-label has to be a single lowercase word."
  }
}

variable "maintainer" {
  type        = string
  description = "The name of the Dev Ops guy."
  default     = "Roman Shchekotov"
}

###########
# Secrets #
###########
variable "cloudflare_token" {
  type        = string
  description = "The Cloudflare API Token"
  sensitive   = true
}

variable "ldap_admin_password" {
  type        = string
  description = "LDAP Admin Password"
  sensitive   = true
}

variable "gitlab_root_password" {
  type        = string
  description = "GitLab Admin Password"
  sensitive   = true
}