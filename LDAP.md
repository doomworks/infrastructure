# OpenLDAP
Some note-worthy LDAP facts, examples and pages.
## Sources
- [[https://wiki.archlinux.org/title/LDAP_authentication][Arch Wiki Page]]
- [[https://ldapwiki.com/][LDAP Wiki]]
- [[https://github.com/osixia/docker-openldap][OpenLDAP Docker Image]]

## Common Queries
- `ldapsearch -b "dc=ldap,dc=local" -D "cn=admin,dc=ldap,dc=local" -w password`